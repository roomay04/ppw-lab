
var value="";
var send = function(event){
  if (event.keyCode == 13){
    document.getElementsByTagName("textarea")[0].value="";
    var box = document.getElementsByClassName("msg-insert")[0];
    var para = document.createElement("P");
    para.setAttribute('class','msg-send');
    console.log(value);
    var t = document.createTextNode(value);
    para.appendChild(t);
    box.appendChild(para);
    console.log("masuk");
    event.preventDefault();
  }else{
    value += event.key;
    console.log(value);
    document.getElementsByClassName("chat-text").value="";
  }
  
}
// Calculator
var print = document.getElementById('print');
var erase = false;

var go = function(x) {
  if (x === 'ac') {
    print.value = "";  
  } else if (x === 'eval') {
      print.value = Math.round(evil(print.value) * 10000) / 10000;
      erase = true;
  } else {
    print.value += x;
  }
};

function evil(fn) {
  return new Function('return ' + fn)();
}
// END
localStorage.setItem("themes", JSON.stringify([
  {"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},
  {"id":1,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},
  {"id":2,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},
  {"id":3,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},
  {"id":4,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},
  {"id":5,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},
  {"id":6,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},
  {"id":7,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},
  {"id":8,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},
  {"id":9,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},
  {"id":10,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"}
]));

localStorage.setItem("selectedTheme", JSON.stringify(
  {"Indigo":{"bcgColor":"#3F51B5","fontColor":"#FAFAFA"}}
));

function applyTheme(selectedTheme) {
  $('body').css('background', selectedTheme.bcgColor);
  $('body').css('color', selectedTheme.fontColor);
  localStorage.setItem('selectedThemeId', JSON.stringify(selectedTheme));
}

$(document).ready(function() {
  applyTheme(JSON.parse(localStorage.getItem('themes'))[3]);
  $('.my-select').select2({
      data: JSON.parse(localStorage.getItem('themes'))
  });
  $('.apply-button').on('click', function(){ 
    theme = JSON.parse(localStorage.getItem('themes'))[$('.my-select').val()];
    applyTheme(theme);
    localStorage.setItem('selectedTheme', JSON.stringify(theme));  
   });
});