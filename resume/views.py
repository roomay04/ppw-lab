from django.shortcuts import render

# Create your views here.
def index(request):
    response={'author':'Hanifa Arrumaisha'}
    return render(request, 'resume/index.html', response)