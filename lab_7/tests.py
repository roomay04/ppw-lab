from django.test import TestCase, Client

# Create your tests here.
from django.urls import resolve
from .views import index
from .models import Friend
from .api_csui_helper.csui_helper import CSUIhelper
from django.core.exceptions import PermissionDenied
# Create your tests here.
class Lab7UnitTest(TestCase):
    def test_lab7_url_is_exist(self):
        response = Client().get('/lab-7/')
        self.assertEqual(response.status_code, 200)

    def test_lab7_using_index_func(self):
        found = resolve('/lab-7/')
        self.assertEqual(found.func, index)
    def test_model_can_create_new_friend(self):
        #Creating a new activity
        new_friend = Friend.objects.create(friend_name='coba aja', npm='172983029', alamat_mhs="mana aja", tgl_lahir="kapan aja", prodi="apa aja", angkatan="hehe")

        #Retrieving all available activity
        counting_all_available_friend = Friend.objects.all().count()
        self.assertEqual(counting_all_available_friend, 1)

    def test_lab7_post_success_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/lab-7/add-friend/', {'name': test, 'npm': 1823902943, 'alamat_mhs':"mana aja", "tgl_lahir":"kapan aja", "prodi":"apa aja", "angkatan":"hehe"})
        self.assertEqual(response_post.status_code, 200)

        response= Client().get('/lab-7/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_friend_list_url_is_exist(self):
        response = self.client.get('/lab-7/get-friend-list/')
        self.assertEqual(response.status_code, 200)

    def test_get_friend_list_data_url_is_exist(self):
        response = Client().get('/lab-7/my-list-friend/')
        self.assertEqual(response.status_code, 200)

    def test_friend_description_url_is_exist(self):
        friend = Friend.objects.create(friend_name="Pina Korata", id=1)
        response = Client().post('/lab-7/get-friend-list/view/'+str(friend.id)+'/')
        self.assertEqual(response.status_code, 200)

    def test_validate_npm(self):
        response = self.client.post('/lab-7/validate-npm/', {'npm': '1606824332'})
        html_response = response.content.decode('utf8')
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(html_response, {'is_taken':False})

    def test_delete_friend(self):
        friend = Friend.objects.create(friend_name="Pina Korata", id=1)
        response = Client().post('/lab-7/get-friend-list/delete-friend/' + str(friend.id) + '/')
        self.assertEqual(response.status_code, 302)
        self.assertNotIn(friend, Friend.objects.all())

    def test_csui_helper_wrong_password(self):
        with self.assertRaises(Exception):
            csui_helper2 = CSUIhelper(username="wrong", password="salah")
    
    def test_csui_helper(self):
        csui_helper = CSUIhelper()
        auth_param = csui_helper.instance.get_auth_param_dict()
        self.assertEqual(auth_param['client_id'], 'X3zNkFmepkdA47ASNMDZRX3Z9gqSU1Lwywu5WepG')

