from django.conf.urls import url
from .views import index, add_friend, validate_npm, delete_friend, friend_list, my_friend_list, view

app_name = "lab_7"

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^add-friend/$', add_friend, name='add-friend'),
    url(r'^validate-npm/$', validate_npm, name='validate-npm'),
    url(r'^get-friend-list/delete-friend/(?P<friend_id>[0-9]+)/$', delete_friend, name='delete-friend'),
    url(r'^get-friend-list/$', friend_list, name='get-friend-list'),
    url(r'^my-list-friend/$', my_friend_list, name='my-friend-list'),
    url(r'^get-friend-list/view/(?P<friend_id>[0-9]+)/$', view, name='view'),
]